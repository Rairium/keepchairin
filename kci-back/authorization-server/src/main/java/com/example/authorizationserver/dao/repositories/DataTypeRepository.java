package com.example.authorizationserver.dao.repositories;

import com.example.authorizationserver.dao.DataType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataTypeRepository extends JpaRepository<DataType,Long> {
    DataType findByName (String name);
}
