package com.example.authorizationserver.services;

import com.example.authorizationserver.dao.DataType;
import com.example.authorizationserver.dao.repositories.DataTypeRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataTypesService {

    private final DataTypeRepository dataTypeRepository;


    public DataTypesService(DataTypeRepository dataTypeRepository) {
        this.dataTypeRepository = dataTypeRepository;
    }

    @PreAuthorize("@authorizationService.checkAuthorization('Role','read').isAuthorized()")
    public List<DataType> getAllDataTypes(){
        return dataTypeRepository.findAll();
    }
}
