package com.example.authorizationserver.services;

import com.example.authorizationserver.dao.User;
import com.example.authorizationserver.utils.AuthenticationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    private final RestTemplate restTemplate;
    public static final String USER_MANAGEMENT_URL = "http://localhost:8089/";

    public UserService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public User getUserManager(String username) {

        HttpHeaders authorizationHeader = AuthenticationUtils.getAuthorizationHeader();
        HttpEntity<Void> request = new HttpEntity<>(authorizationHeader);

        ResponseEntity<User> response = restTemplate.exchange(USER_MANAGEMENT_URL + "/users/{username}/manager", HttpMethod.GET, request, User.class, username);

        return response.getBody();

    }
}
