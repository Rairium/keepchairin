package com.example.notificationserver.services;


import com.example.notificationserver.model.dao.SubscriptionDao;

import java.util.List;
import java.util.Optional;

public interface SubscriptionService {

	List<SubscriptionDao> findAllPushNotifications();

	Optional<SubscriptionDao> findByUsername(String username);

	List<SubscriptionDao> getSubscriptionsFromDB(String username);

	Optional<SubscriptionDao> deleteByUsername(String username);

	SubscriptionDao save(SubscriptionDao pushNotification);

	void deleteById(Long id);

}
