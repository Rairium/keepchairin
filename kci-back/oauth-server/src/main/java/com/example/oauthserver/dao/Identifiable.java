package com.example.oauthserver.dao;

import java.io.Serializable;

public interface Identifiable {
    public Serializable getId();
}
