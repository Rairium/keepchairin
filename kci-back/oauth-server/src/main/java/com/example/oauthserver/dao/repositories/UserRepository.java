package com.example.oauthserver.dao.repositories;

import com.example.oauthserver.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String name);

    boolean existsById(Long id);

    boolean existsByUsername(String username);

}
