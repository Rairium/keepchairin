package com.example.placemanagement.data;

import java.io.Serializable;

public interface Identifiable {
    public Serializable getId();
}
