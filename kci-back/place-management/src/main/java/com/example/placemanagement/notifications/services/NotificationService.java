package com.example.placemanagement.notifications.services;

import com.example.placemanagement.authorization.AuthorizationUtils;
import com.example.placemanagement.data.entities.Place;
import com.example.placemanagement.data.entities.PlaceRequest;
import com.example.placemanagement.notifications.model.dto.ModifiedPlace;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

	private PlaceNotificationBuilderProxyFeign feignProxy;

	public NotificationService(PlaceNotificationBuilderProxyFeign feignProxy) {
		this.feignProxy = feignProxy;
	}

	public void sendNewPlaceRequestManagerNotification(PlaceRequest placeRequest) {

		feignProxy.sendNewPlaceRequestManagerNotification(placeRequest, AuthorizationUtils.getAuthToken());
	}

	public void sendReviewedPlaceRequestEmployeeNotification(PlaceRequest placeRequest) {

		feignProxy.sendReviewedPlaceRequestEmployeeNotification(placeRequest, AuthorizationUtils.getAuthToken());
	}

	public void sendModifiedPlaceNotification(Place newPlace) {

		feignProxy.sendModifiedPlaceNotification(newPlace, AuthorizationUtils.getAuthToken());
	}

//	public ModifiedPlace compactToPlaceHolder(Place newPlace) {
//		ModifiedPlace modifiedPlace = new ModifiedPlace();
//		modifiedPlace.setPlace(newPlace);
//
//		return modifiedPlace;
//	}


}
