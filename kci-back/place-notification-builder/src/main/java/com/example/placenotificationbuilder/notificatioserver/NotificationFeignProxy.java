package com.example.placenotificationbuilder.notificatioserver;

import com.example.placenotificationbuilder.model.dto.NotificationBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "place-notification-builder", url ="localhost:8087" )
public interface NotificationFeignProxy {
	@Autowired
	@PostMapping("/api/v01/notifications/{receiver}")
	 ResponseEntity sendNotificationJSON(@PathVariable("receiver") String receiver,
										 @RequestBody NotificationBuilder objectJSON,
										 @RequestHeader("Authorization") String token);
}
