package com.example.placenotificationbuilder.notificatioserver;

import com.example.placenotificationbuilder.model.dto.NotificationBuilder;
import com.example.placenotificationbuilder.utils.AuthorizationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationServerService {

	private NotificationFeignProxy feignProxy;

	public NotificationServerService(NotificationFeignProxy feignProxy) {
		this.feignProxy = feignProxy;
	}

	public void sendNotificationJSON(String receiver, NotificationBuilder objectJSON) {

		feignProxy.sendNotificationJSON(receiver, objectJSON, AuthorizationUtils.getAuthToken());
	}
}





