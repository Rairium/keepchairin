package com.example.usermanagement.dao;

import java.io.Serializable;

public interface Identifiable {
    public Serializable getId();
}
