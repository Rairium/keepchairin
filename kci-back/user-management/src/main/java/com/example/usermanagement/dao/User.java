package com.example.usermanagement.dao;

import com.example.usermanagement.authentication.UserAuthenticationListener;
import com.example.usermanagement.authorization.UserAuthorizationListener;
import lombok.AllArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@Table(name = "users")
@EntityListeners({UserAuthorizationListener.class, UserAuthenticationListener.class})

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @NaturalId
    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @ManyToOne(fetch = FetchType.EAGER)
    private User manager;

    @Transient
    private Role role;

    @Transient
    private String password;

    //region Constructors
    public User() {
    }

    public User(String firstName, String lastName, String email, String username, User manager, Role role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.manager = manager;
        this.role = role;
    }
    //endregion

    //region Getters and Setters
    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getfirstName() {
        return firstName;
    }

    public User setfirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getlastName() {
        return lastName;
    }

    public User setlastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public User getManager() {
        return manager;
    }

    public User setManager(User manager) {
        this.manager = manager;
        return this;
    }

    public Role getRole() {
        return role;
    }

    public User setRole(Role role) {
        this.role = role;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }
    //endregion
}
