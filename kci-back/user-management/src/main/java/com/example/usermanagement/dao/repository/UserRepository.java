package com.example.usermanagement.dao.repository;

import com.example.usermanagement.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByFirstNameContainingOrLastNameContaining(String firstArg, String secondArg);

    List<User> findUsersByManagerUsername(String firstArg);

    User findUserByUsername(String firstArg);

}

