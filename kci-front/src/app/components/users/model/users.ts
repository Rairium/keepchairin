export interface UserModel {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
  manager: {
    firstName: string;
  };
  role: Role;
  }

export interface Role {
  id: string;
  name: string;
  enabled: boolean;
}

export interface UserViewModel {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  manager: string;
  role: string;
  password: string;
}
